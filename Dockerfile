FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9

COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

COPY . /app
WORKDIR /app/

CMD celery -A app.celery:celery_app worker --loglevel=INFO --pool=solo

ENV PYTHONPATH=/app
EXPOSE 80