from fastapi import APIRouter

from app.api.resize import resize_task_router

api_router = APIRouter()


api_router.include_router(resize_task_router, prefix="/resize", tags=["resize"])
