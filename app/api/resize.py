from uuid import uuid4

from fastapi import (APIRouter, BackgroundTasks, Depends, HTTPException,
                     UploadFile, status)

from app.db import get_db
from app.db.images import ImagesInfo
from app.helpers import is_correct_format
from app.helpers.drag import process_file
from app.schemas.tasks import GetTask, PostTask

resize_task_router = APIRouter()


@resize_task_router.post(
    path="", status_code=status.HTTP_200_OK, response_model=PostTask
)
async def post_image_with_parametrs(
    uploaded_image: UploadFile,
    width: int,
    height: int,
    backgroun_tasks: BackgroundTasks,
    mongo_db=Depends(get_db),
):
    image_name = uploaded_image.filename
    if not is_correct_format(image_name):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The file has an invalid extension",
        )

    new_file_id = str(uuid4())

    backgroun_tasks.add_task(
        process_file,
        db=mongo_db,
        image=uploaded_image,
        file_id=new_file_id,
        new_width=width,
        new_height=height,
    )

    return PostTask(task_id=new_file_id)


@resize_task_router.get(
    path="/{task_id}", status_code=status.HTTP_200_OK, response_model=GetTask
)
async def get_image_by_id(task_id: str, mongo_db=Depends(get_db)):
    image_info = await ImagesInfo.get_image_info_by_id(db=mongo_db, id=task_id)

    if image_info:
        return image_info
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="The image doesn't exist",
        )
