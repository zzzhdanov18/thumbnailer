from celery import Celery

from app.config import settings

celery_app = Celery("worker", broker=settings.REDIS_URL)

celery_app.autodiscover_tasks(["app.celery.tasks"])
