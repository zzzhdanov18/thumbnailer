from pymongo import MongoClient

from app.config import settings

client = MongoClient(settings.MONGODB_CONNECTION_URL)


def get_mongo_db(db_name):
    return client[db_name]
