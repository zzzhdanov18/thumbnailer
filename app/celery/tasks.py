from datetime import datetime

from celery import shared_task

from app.celery.helpers.tasks import get_mongo_db
from app.config import settings
from app.db.images import ImagesInfo
from app.helpers import get_bytes_of_resized_image
from app.s3.storage import YStorage


@shared_task
def resize_and_upload(database_name, name, width, height):
    mongo_database = get_mongo_db(database_name)
    file_id = name.split(".")[0]

    try:
        ImagesInfo.update_image_info_sync(
            db=mongo_database, id=file_id, new_status="STARTED"
        )

        content = YStorage.get_object_bytes(key=name)

        resized_bytes_image = get_bytes_of_resized_image(
            image=content, new_width=width, new_heihgt=height
        )
        new_image_name = f"res_{name}"

        YStorage.upload_image(content=resized_bytes_image, key=new_image_name)
    except Exception:
        ImagesInfo.update_image_info_sync(
            db=mongo_database, id=file_id, new_status="ERROR"
        )

    resized_url = (
        f"{settings.STORAGE_URL}/{settings.STORAGE_MAIN_BUCKET}/{new_image_name}"
    )
    end_time = datetime.today()

    ImagesInfo.update_image_info_sync(
        db=mongo_database,
        id=file_id,
        new_status="SUCCESS",
        url=resized_url,
        completion_time=end_time,
    )
