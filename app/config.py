from pydantic import BaseSettings


class Settings(BaseSettings):
    MONGO_DB: str = "test"
    MONGO_TEST_DB = "PYTEST_DB"
    MONGODB_CONNECTION_URL: str = ""
    REDIS_URL: str = ""

    AWS_ACCESS_KEY_ID: str = ""
    AWS_SECRET_ACCESS_KEY: str = ""
    WS_DEFAULT_REGION: str = ""

    STORAGE_URL = "https://storage.yandexcloud.net"
    STORAGE_MAIN_BUCKET = "testintern"

    CELERY_DB = "backend_celery"
    CELERY_COLLECTION = "tasks"

    class Config:
        case_sensitive = True
        env_file = ".env"


settings = Settings()
