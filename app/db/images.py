from datetime import datetime

from motor.motor_asyncio import AsyncIOMotorDatabase
from pymongo import MongoClient

from app.schemas.tasks import GetTask

collection = "images"


class ImagesInfo:
    @staticmethod
    async def create_image_info(db: AsyncIOMotorDatabase, id: str) -> None:
        new_image = GetTask(
            task_id=id,
            task_status="PENDING",
            created_at=datetime.today(),
            finished_at=None,
            image_url="",
        )

        await db[collection].insert_one(new_image.dict())

    @staticmethod
    async def get_image_info_by_id(db: AsyncIOMotorDatabase, id: str) -> dict:
        query = {"task_id": id}

        result = await db[collection].find_one(query)
        return result

    @staticmethod
    def update_image_info_sync(
        db: MongoClient,
        id: str,
        url: str = "",
        completion_time: datetime = None,
        new_status: str = "PENDING",
    ) -> None:
        db[collection].update_one(
            {"task_id": id},
            {
                "$set": {
                    "task_status": new_status,
                    "image_url": url,
                    "finished_at": completion_time,
                }
            },
        )
