from io import BytesIO

from PIL import Image


def get_bytes_of_resized_image(
    image: bytes, new_width: int, new_heihgt: int
) -> BytesIO:
    with Image.open(image) as primary_image:
        resized_image = primary_image.resize((new_width, new_heihgt))
        image_byte_content = BytesIO()
        resized_image.save(image_byte_content, format=primary_image.format)
        image_byte_content.seek(0)

    return image_byte_content


def is_correct_format(primary_name: str) -> bool:
    image_formats = {"png", "jpg", "jpeg", "bmp"}
    file_format = primary_name.split(".")[-1].lower()
    return file_format in image_formats
