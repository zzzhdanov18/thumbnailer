from fastapi import UploadFile
from motor.motor_asyncio import AsyncIOMotorDatabase

from app.celery.tasks import resize_and_upload
from app.db.images import ImagesInfo
from app.s3.storage import YStorage


async def process_file(
    db: AsyncIOMotorDatabase,
    image: UploadFile,
    file_id: str,
    new_width: int,
    new_height: int,
):
    """Uploads the original image to the storage,
    creates an entry in MongoDB, then adds celery-task for image-resizing

    Params
    ------
    db: AsyncIOMotorDatabase
        MongoDB client
    image: UploadFIle
        Uploaded via FastAPI file
    file_id: str
        Future file ID (UUID)
    new_width: int
        New width value
    new_height: int
        New height value

    Return
    ------
        None

    """
    file_format = image.filename.split(".")[-1]
    file_name = f"{file_id}.{file_format}"
    file_obj = image.file

    YStorage.upload_image(content=file_obj, key=file_name)
    await ImagesInfo.create_image_info(db=db, id=file_id)
    resize_and_upload.apply_async(
        (db.name, file_name, new_width, new_height), task_id=file_id
    )
