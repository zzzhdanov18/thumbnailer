import logging.config

from fastapi import FastAPI


from app.api import api_router
from app.config import settings
from app.db import (close_mongodb_connection, close_redis_connection,
                    connect_to_mongodb, connect_to_redis)

log_conf = {
    "version": 1,
    "formatters": {
        "default": {
            "format": "%(asctime)s - %(process)s - %(name)s - %(levelname)s - %(message)s"
        }
    },
    "handlers": {
        "console": {
            "formatter": "default",
            "class": "logging.StreamHandler",
            "stream": "ext://sys.stdout",
            "level": "DEBUG",
        }
    },
    "root": {"handlers": ["console"], "level": "DEBUG"},
    "loggers": {
        "gunicorn": {"propagate": True},
        "uvicorn": {"propagate": True},
        "uvicorn.access": {"propagate": True},
        "events": {"propagate": True},
        "requests": {"propagate": True},
        "tasks": {"propagate": True},
    },
}
logging.config.dictConfig(log_conf)


app = FastAPI(
    docs_url="/api/docs", redoc_url="/api/redoc", openapi_url="/api/openapi.json"
)


app.add_event_handler("startup", connect_to_mongodb)
app.add_event_handler("startup", connect_to_redis)
app.add_event_handler("shutdown", close_mongodb_connection)
app.add_event_handler("shutdown", close_redis_connection)

app.include_router(api_router, prefix="/api")


