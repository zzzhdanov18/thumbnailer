import boto3
from dotenv import load_dotenv

from app.config import settings

load_dotenv()

session = boto3.session.Session()

s3 = session.client(
    service_name="s3",
    endpoint_url=settings.STORAGE_URL,
)
