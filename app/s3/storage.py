from io import BytesIO
from typing import Optional

from app.config import settings
from app.s3 import s3


class YStorage:
    @staticmethod
    def upload_image(content: bytes, key: str) -> None:
        s3.put_object(Body=content, Bucket=settings.STORAGE_MAIN_BUCKET, Key=key)

    @staticmethod
    def get_object_bytes(key: str) -> Optional[bytes]:
        try:
            request = s3.get_object(Bucket=settings.STORAGE_MAIN_BUCKET, Key=key)[
                "Body"
            ].read()
        except Exception:
            return None
        return BytesIO(request)
