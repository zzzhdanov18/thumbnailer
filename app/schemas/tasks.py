from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field


class PostTask(BaseModel):
    task_id: str = Field(..., title="Task ID")


class GetTask(PostTask):
    task_status: str = Field(..., title="Task status")
    created_at: datetime = Field(..., title="Creation time")
    finished_at: Optional[datetime] = Field(..., title="Completion time")
    image_url: str = Field(..., title="Image URL")
