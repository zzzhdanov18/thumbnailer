from app.config import settings
from app.db import get_db, Database
from app.main import app


from motor.motor_asyncio import AsyncIOMotorClient
from fastapi.testclient import TestClient
import pytest, asyncio, pymongo



async_db = Database()
async_db.client = AsyncIOMotorClient(
    settings.MONGODB_CONNECTION_URL
)


async def override_get_db():
    return async_db.client[settings.MONGO_TEST_DB]

app.dependency_overrides[get_db] = override_get_db


@pytest.fixture(scope="session")
def event_loop(request):
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="session", autouse=True)
def drop_database():
    yield
    client = pymongo.MongoClient(settings.MONGODB_CONNECTION_URL)
    db = client[settings.MONGO_TEST_DB]
    for collection_name in db.list_collection_names():
        db[collection_name].drop()


@pytest.fixture(scope='session')
def celery_config():
    return {
        'broker_url': settings.REDIS_URL
    }





