from PIL import Image
import os

class InputTestData:
    width: int = 200
    height: int = 200

class ExpectedTestData:
    id: int
    url: str
    status: str = "SUCCESS"

data_in = InputTestData()
data_exp = ExpectedTestData()

def test_invalid_extension(client, celery_session_worker):
    file_path = "tests/p.pdf"
    Image.new('RGB', (400, 400), (255, 200, 255)).save(file_path)

    with open(file_path,'rb') as buff_file:
        _files = {'uploaded_image': buff_file}
        response = client.post('/api/resize/',
                                files=_files,
                                params = {
                                    "width": data_in.width,
                                    "height": data_in.height
                                }
                                )
    
    assert response.status_code == 400
    os.remove(file_path)


def test_post_image(client, celery_session_worker):
    file_path = "tests/i.jpg"
    Image.new('RGB', (400, 400), (255, 200, 255)).save(file_path)

    with open(file_path,'rb') as buff_image:
        _files = {'uploaded_image': buff_image}
        response = client.post('/api/resize/',
                                files=_files,
                                params = {
                                    "width": data_in.width,
                                    "height": data_in.height
                                }
                                )
    
    assert response.status_code == 200
    data_exp.id = response.json()["task_id"]
    os.remove(file_path)



