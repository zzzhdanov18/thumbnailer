from tests.test_1_post_image import data_in, data_exp
from urllib.request import urlopen
import io
from PIL import Image

import time

def test_get_nonexistent_image(client):
    nonex_id = "nonexistent"
    response = client.get(f'/api/resize/{nonex_id}')
    assert response.status_code == 404


def test_get_image(client):
    time.sleep(10)
    response = client.get(f'/api/resize/{data_exp.id}')
    data = response.json()

    data_exp.url = data["image_url"]

    assert response.status_code == 200
    assert data["task_status"] == "SUCCESS"


def test_image_link():
    response = urlopen(data_exp.url)
    image_data = response.read()

    image_stream = io.BytesIO(image_data)

    with Image.open(image_stream) as image:
        width, height = image.size
    
    assert width == data_in.width
    assert height == data_in.height